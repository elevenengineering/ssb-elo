$(document).ready(function(){
    $('#btnSwap').click(function(){
      var swap = function(first, second) {
        var f = $('#formAddMatch [name=' + first + ']');
        var s = $('#formAddMatch [name=' + second + ']');
        var f_val = f.val();
        f.val(s.val());
        s.val(f_val);
      };
      swap('winner', 'loser');
      swap('winner_char', 'loser_char');
    });
});
