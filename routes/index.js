var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'SSB ELO' });
});


/* GET Standings page. */
router.get('/standings', function(req, res) {
  var db = req.db;
  var _ = req._;

  var users = db.get('users');
  var matches = db.get('matchhistory');
  
  var matchhistory, userlist;

  var render = function() {
    var data = [];
    var sorted_users = userlist.slice().sort(function(a, b) {
      return a.username.localeCompare(b.username);
    });
    sorted_users.map(function(user) {
      user.wins = 0;
      user.losses = 0;
      var elo = 1500;
      var x = 1;
      var elo_data = [elo];
      var x_data = [x];
      matchhistory.forEach(function(match) {
        if (user.username === match.winner) {
          user.wins += 1;
          elo += match.elo_delta;
        } else if (user.username === match.loser) {
          user.losses += 1;
          elo -= match.elo_delta;
        }
        elo_data.push(elo);
        x_data.push(++x);
      });
      data.push({
        x: x_data,
        y: elo_data,
        mode: 'lines',
        type: 'scatter',
        name: user.username
      });
      return user;
    });
    console.log(data);
    res.render('standings', {
        "users" : userlist,
        "data" : JSON.stringify(data),
    });
  };

  users.find({},{sort: {elo: -1}},function(e,docs){
    userlist = docs;
    finished();
  });

  matches.find({},{},function(e, docs) {
    matchhistory = docs;
    finished();
  });

  var finished = _.after(2, render);

});

/* GET Match History page. */
router.get('/matchhistory', function(req, res) {
  var db = req.db;
  var _ = req._;

  var users = db.get('users');
  var matches = db.get('matchhistory');

  var matchhistory, userlist;
  var character_list = [
    "Bowser",
    "Captain Falcon",
    "Donkey Kong",
    "Dr. Mario",
    "Falco",
    "Fox",
    "Ganondorf",
    "Ice Climbers",
    "Jigglypuff",
    "Kirby",
    "Link",
    "Luigi",
    "Mario",
    "Marth",
    "Mewtwo",
    "Mr. Game & Watch",
    "Ness",
    "Peach",
    "Pichu",
    "Pikachu",
    "Roy",
    "Samus",
    "Sheik",
    "Yoshi",
    "Young Link",
    "Zelda",
  ];


  var render = function() {
    var pers = {};
    pers.winner = req.cookies.winner || userlist[0].username;
    pers.loser = req.cookies.loser || userlist[1].username;
    pers.winner_char = req.cookies.winner_char || character_list;
    pers.loser_char = req.cookies.loser_char || character_list;
    res.render('matchhistory', {
      "matchhistory" : matchhistory,
      "users" : userlist,
      "characters" : character_list,
      "pers" : pers
    });
  };

  var finished = _.after(2, render);

  matches.find({},{sort: {date: -1}},function(e,docs){
    matchhistory = docs;
    finished();
  });
  users.find({},{sort: {username: 1}},function(e,docs){
    userlist = docs;
    finished();
  });

});

/* GET Remove Match Service */
router.get('/removematch', function(req, res) {
  var db = req.db;
  var _ = req._;
  var users = db.get('users');
  var matches = db.get('matchhistory');

  var render = function() {
    res.redirect('matchhistory');
  };
  var updateUser = function(user, elo) {
    users.findAndModify({
      "query" : { "username" : user},
      "update" : { "$inc": { "elo": elo} }
    }, function(e, docs) {});
    finished();
  };

  matches.find({}, { limit: 1, sort:{date:-1}}, function(e,docs) {
    if (Object.keys(docs).length == 1) {
      matches.remove({ "_id": docs[0]._id});
      updateUser(docs[0].winner, -docs[0].elo_delta);
      updateUser(docs[0].loser, +docs[0].elo_delta);
    } else {
      render();
    }
  });

  var finished = _.after(2, render);
});

/* POST to Add Match Service */
router.post('/addmatch', function(req, res) {
  // Import packages
  var db = req.db;
  var _ = req._;
  var elo = req.elo;

  // db connections
  var users = db.get('users');
  var matches = db.get('matchhistory');

  // Local variables, populated by async routines
  var winner = req.body.winner;
  var loser = req.body.loser;
  var three_stock = req.body.three_stock;
  var winner_char = req.body.winner_char;
  var loser_char = req.body.loser_char;
  var elos = { "winner" : 0, "loser" : 0};
  var err = "";

  if (winner === loser)
    err = "Winner cannot be the same as Loser!";

  // Callbacks and helper functions
  var findElo = function(which) {
    return function(e, docs) {
      if (Object.keys(docs).length != 1) {
        err += which + " field was not found\n";
      } else {
        elos[which] = +docs[0].elo;
      }
      finished();
    }
  };
  var updateUser = function(user, elo) {
    users.findAndModify({
      "query" : { "username" : user},
      "update" : { "$set": { "elo": elo} }
    }, function(e, docs) {});
  };
  var updateAndRender = function() {
    if (err !== "") {
      res.send(err);
    } else {
      var winner_elo = elos["winner"];
      var loser_elo = elos["loser"];
      var exp_winner = elo.getExpected(winner_elo, loser_elo);
      var elo_delta = elo.updateRating(exp_winner, 1, winner_elo) - winner_elo;

      // Submit to the DB
      matches.insert({
        "winner" : winner,
        "winner_char" : winner_char,
        "loser" : loser, 
        "loser_char" : loser_char,
        "elo_delta" : elo_delta,
        "three_stock" : three_stock,
        "date" : new Date()
      }, function(e,docs){});

      res.cookie('winner', winner);
      res.cookie('winner_char', winner_char);
      res.cookie('loser', loser);
      res.cookie('loser_char', loser_char);

      updateUser(winner, winner_elo + elo_delta);
      updateUser(loser, loser_elo - elo_delta);

      res.redirect("matchhistory");
    }
  };

  // Business logic starts here
  var finished = _.after(2, updateAndRender);

  users.find({ username: winner }, {}, findElo("winner"));
  users.find({ username: loser }, {}, findElo("loser"));
});

module.exports = router;
